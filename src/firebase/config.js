import { initializeApp } from "firebase/app";
import { getFirestore } from "@firebase/firestore";
 
const firebaseConfig = {
    apiKey: "AIzaSyAw_fqetRiOZ-wxqTCCiQ0RYrvSZVLy8RU",
    authDomain: "app-intercorp.firebaseapp.com",
    projectId: "app-intercorp",
    storageBucket: "app-intercorp.appspot.com",
    messagingSenderId: "399653037818",
    appId: "1:399653037818:web:a7d08c6733349ff6724b43"
};

const app = initializeApp(firebaseConfig);

export const db = getFirestore(app);