import { Provider } from 'react-redux';
import { BrowserRouter } from "react-router-dom";

import { AppRouter } from "./router/AppRouter";
import { Modal, Navbar } from "./components";
import { store } from "./store";

export const App = () => {
  return (
    <Provider store={ store }>
      <BrowserRouter>
        <Navbar />
        <AppRouter />
      </BrowserRouter>
    </Provider>
  )
}
