import { useDispatch, useSelector } from 'react-redux';
import { onCloseModal, onOpenModal, onSetActiveEvent,  } from '../store';

export const useClientStore = () => {

    const dispatch = useDispatch();

    const { clients } = useSelector( state => state.client);

    const setActiveEvent = ( clientEvent ) => {
        dispatch( onSetActiveEvent( clientEvent ) );
    }

    return {
        // Properties
        clients,

        // Methods
        setActiveEvent
    }
}
