import * as React from 'react';

import {Button, Tooltip, IconButton} from '@mui/material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';

import Swal from 'sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

import { useEffect, useState } from 'react';

/* Firebase */
import { db } from '../../firebase/config';
import { collection, deleteDoc, doc, getDocs } from 'firebase/firestore';
import { useClientStore } from '../../hooks';

export const DataTable = () => {

  const { setActiveEvent } = useClientStore();
  
  const [users, setUsers] = useState([]);
  const usersCollectionRef = collection(db, "Users");

  const getUsers = async () => {
    const data = await getDocs(usersCollectionRef);

    setUsers(data.docs.map( (doc) => ({ ...doc.data(), id: doc.id })));
  };

  const deleteClient = async (id) => {
    const clientDoc = doc(db, "Users", id);

    await deleteDoc(clientDoc);
    getUsers();
    
  }

  const confirmDelete = (id)=> {
    Swal.fire({
      title: '<h4>¿Seguro de eliminar al cliente?</h4>',
      showDenyButton: true,
      showCancelButton: false,
      confirmButtonText: 'Delete Client',
      denyButtonText: `Close Modal`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        deleteClient(id);
        Swal.fire('Saved!', '', 'success');
      } else if (result.isDenied) {
        Swal.close();
      }
    })
  }


  useEffect(() => {
    getUsers();
  }, [])

  const convertToDate = (date) => {
    var today = new Date(date).toLocaleDateString('ES', {
        day : 'numeric',
        month : 'short',
        year : 'numeric'
    }).split(' ').join(' ');

    return today;
  }

  return (

    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="center">Name</TableCell>
            <TableCell align="center">Last Name</TableCell>
            <TableCell align="center">Date of Birth</TableCell>
            <TableCell align="center">Age</TableCell>
            <TableCell align="center">Actions</TableCell>

          </TableRow>
        </TableHead>
        <TableBody>

        {users.map((user) => {
          return (
            <TableRow
              key={user.id}
            >
              <TableCell align="center" component="th" scope="row">{user.name}</TableCell>
              <TableCell align="center" component="th" scope="row">{user.last_name}</TableCell>
              <TableCell align="center" component="th" scope="row">{convertToDate(user.date_birth)}</TableCell>
              <TableCell align="center" component="th" scope="row">{user.age}</TableCell>

              <TableCell align="center" component="th" scope="row">
                <Tooltip title="Delete">
                  <IconButton className='btn-delete' onClick={ ()=> {confirmDelete(user.id) }}>
                    <DeleteOutlineIcon />
                  </IconButton>
                </Tooltip>
              </TableCell>

            </TableRow>
          )
        })}
        </TableBody>
      </Table>
    </TableContainer>
    
  )
}
