import { Typography, Grid } from '@mui/material';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';

import './style.css';

export const Title = ({title}) => {
  return (
    <Grid container spacing={2} className="title">
      <Grid 
        item xs={12} md={12}
        mb={2}
        display="flex"
        >
        <ArrowRightIcon></ArrowRightIcon>

        <Typography 
          variant="h5" 
          component="h5"
          fontWeight={600}>
          
          {title}
        </Typography>
      </Grid>

    </Grid>
  )
}
