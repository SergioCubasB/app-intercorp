import { Button } from '@mui/material';
import { useLocation } from 'react-router-dom'

import { Link } from "react-router-dom";

import DonutSmallIcon from '@mui/icons-material/DonutSmall';
import KeyboardReturnIcon from '@mui/icons-material/KeyboardReturn';

import { Modal } from '../Modal/Modal';

import './style.css';

export const Navbar = () => {
  const route = ()=> {
    const getRoute = useLocation();
    return  getRoute.pathname;
  }

  return (
    <>
    <div className="container-navbar">
        <div className="container-navbar__logo">
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQODcINmZdbcAMadUciO6qAOdyR-F_hSHwNLwCj2rj7i1jw8h4tfaWBFGhg0iufRCa3jw&usqp=CAU" alt="" />
        </div>

        <div className="container-navbar__actions">
          {route() != '/metric' &&
            <Link to="/metric">
                <Button variant="outlined" startIcon={<DonutSmallIcon />}>
                  Show Metrics
                </Button>
            </Link>
          }

          {route() != '/client' &&
            <Link to="../client">
                <Button variant="outlined" startIcon={<KeyboardReturnIcon />}>
                  Return to Clients
                </Button>
            </Link>
          }
        </div>
    </div>
    </>
  )
}
