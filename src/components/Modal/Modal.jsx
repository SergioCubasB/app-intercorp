import React, { useMemo, useState } from 'react';

import Swal from 'sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useUiStore } from '../../hooks/useUiStore';


import { db } from '../../firebase/config';
import { addDoc, collection } from 'firebase/firestore';


import './style.css';

export const Modal = ( {title = '' }) => {
  const usersCollectionRef = collection(db, "Users");
  const { isModalOpen, closeModal } = useUiStore();

  const [formSubmitted, setformSubmitted] = useState(false);

  const [ formValues, setFormValue ] = useState({
    name: '',
    last_name: '',
    dateOfBirth: ''
  });

  const nameValidate = useMemo( () => {
    if(!formSubmitted) return '';

    return ( formValues.name.length > 0) ? '' : 'error';

  }, [ formValues.name, formSubmitted ]);


  const onInputChanged = ( { target } ) => {
    setFormValue({
      ...formValues,
      [ target.name ]: target.value
    })
  };

  const onSubmit = ( event ) => {
    event.preventDefault();
    setformSubmitted(true);

    const { name, last_name, dateOfBirth } = formValues;

    if ( dateOfBirth.length <=0 ) return;

    if ( name.length <=0 ) return;

    if ( last_name.length <=0 ) return;

    addDoc(usersCollectionRef, { name: name, last_name:last_name, date_birth:dateOfBirth, age:setAge(dateOfBirth)});

    handleClose();

    Swal.fire(
      '',
      'Cliente creado exitosamente.',
      'success'
    );

  }

  const setAge = (date) => {
    const today = new Date();
    const birthDate = new Date(date);
    const age = today.getFullYear() - birthDate.getFullYear();

    return age; 
  }

  const handleClose = () => {
      closeModal();
  };


  return (
    <Dialog open={ isModalOpen } onClose={handleClose}>
        <DialogTitle> { title }</DialogTitle>
        <DialogContent>
          <form onSubmit={onSubmit}>
            <div className="form-group">

              <TextField 
                fullWidth 
                label="Name"
                name="name"
                value={ formValues.name }
                onChange= { onInputChanged }
              />
            </div>

            <div className="form-group">
              <TextField 
                fullWidth 
                label="Full Name" 
                name="last_name"
                value={ formValues.last_name }
                onChange= { onInputChanged }

              />
            </div>

            <div className="form-group">
              <TextField
                fullWidth 
                name='dateOfBirth'
                type="date"
                onChange= { onInputChanged }
              />
            </div>

            <div className="form-group">
              <Button variant="outlined" onClick={handleClose}>
                Close
              </Button>
              <Button variant="contained" type='submit'>
                Send
              </Button>
            </div>
            
          </form>

        </DialogContent>
    </Dialog>
  )
}
