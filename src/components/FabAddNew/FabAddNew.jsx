import AddIcon from '@mui/icons-material/Add';
import { Grid, Fab, Tooltip } from '@mui/material';
import { useUiStore } from '../../hooks';

export const FabAddNew = () => {

    const { openModal } = useUiStore();

    const onOpenModal = ( event ) => {
      openModal();
    };

    
  return (

    <Tooltip title="Add client">
      <Fab color="primary" aria-label="add" onClick={ onOpenModal }>
          <AddIcon />
      </Fab>
    </Tooltip>
    
  )
}
