import { ClientePage } from "../Client/ClientePage"

  export const PageRoutes = () => {
    return (
        <Routes>
            <Route path="/" element={ <ClientePage />} /> 
            <Route path="/metric" element={ <ClientePage />} /> 

            <Route path="/*" element={ <Navigate to="/client" />} />
        </Routes>
    )
  }
  