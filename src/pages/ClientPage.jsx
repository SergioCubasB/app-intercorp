import { Grid } from '@mui/material';
import { useEffect, useState } from 'react';


import { Title } from '../components/Title/Title';
import { DataTable } from '../components/DataTable/DataTable';

import { FabAddNew } from '../components/FabAddNew/FabAddNew';
import { Modal } from '../components/Modal/Modal';

export const ClientePage = () => {

  return (
    <Grid container
    padding={8}>
      <Grid item width={"100%"}>
        <Title title={"Lista de Clientes"}/>
      </Grid>

      <Grid width={"100%"} >
        <DataTable />
      </Grid>

      <Grid 
        position={"fixed"}
        left="auto"
        right="20px"
        bottom="20px"
      >
        <FabAddNew />
      </Grid>

      <Modal title='Add Client'/>

    </Grid>
  )
}
