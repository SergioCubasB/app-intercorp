import { Routes, Route, Navigate } from "react-router-dom";

import { ClientePage } from "../pages/ClientPage"
import { MetricPage } from "../pages/MetricPage"

export const AppRouter = () => {
  return (
    <Routes>
      <Route path="/*" element={ <Navigate to="/client" />} /> 
      <Route path="/metric/*" element={ <MetricPage />} /> 
      <Route path="/client/*" element={ <ClientePage />} /> 

    </Routes>
  )
}
