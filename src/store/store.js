import { configureStore } from '@reduxjs/toolkit';
import { uiSlice, clientSlice } from './';

export const store = configureStore({
    reducer: {
        ui: uiSlice.reducer,
        client: clientSlice.reducer
    }
})