import { createSlice } from "@reduxjs/toolkit";

const client = {
    name: 'Sergio',
    last_name: 'Cubas BErnal',
    dateOfBirth: '11-06-1996'
};

export const clientSlice = createSlice({
    name: 'client',
    initialState: {
        clients: [
            client
        ],
        activeEvent: null
    },
    reducers: {
        onSetActiveEvent: ( state, { payload } ) => {
            state.activeEvent = payload;
        }
    }
});

export const { onSetActiveEvent } = clientSlice.actions;  
